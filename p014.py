answer = 0
longest = 0

for i in range(1,1000000):
    print("Testing " + str(i))
    sequence = [i]
    number = i
    while number != 1: 
        if number % 2 == 0:
            number //= 2
            sequence.append(number)
        else:
            number = 3 * number + 1
            sequence.append(number)
    if len(sequence) > longest:
        longest = len(sequence)
        answer = i

print(answer)
