def is_palindrome(x):
    return(str(x) == str(x)[::-1])
    
answer = 0
for x in range(99,999):
    for y in range(99,999):
        product = x * y
        if is_palindrome(product) and product > answer:
            answer = product

print(answer) 
