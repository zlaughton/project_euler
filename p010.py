def is_prime(x):
    if x % 2 == 0:
        return False
    max = int(x**0.5)+1
    for i in range(3,max,2):
        if x % i == 0:
            return False
    return True

sum = 2

for i in range(3,2000000,2):
    print(i)
    if is_prime(i):
        sum += i

print(sum)
