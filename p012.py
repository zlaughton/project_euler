def triangle_number(x):
    sum = 0
    for i in range(x):
        sum += (i + 1)
    return sum

def list_factors(x):    # Return a list of factors for a number
    factors = []
    for i in range(1, int(x**0.5)+1):
        if x % i == 0:
            factors.extend((i,x//i))
    return set(factors)

i = 0
factors = []
while len(factors) <= 500:
    i += 1
    triangle = triangle_number(i)
    factors = list_factors(triangle)
    
print(triangle)
