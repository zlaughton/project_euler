ans = i = 20

def divisible_by_20_fac(x):
    divisors = [20, 19, 18, 17, 16, 15, 14, 13, 12, 11]
    for d in divisors:
        if x % d:
            return False
    return True

while divisible_by_20_fac(ans) == False:
    ans+=20
    print("Testing " + str(ans))

print(ans)
