# Project Euler, Problem 17
#
# If the numbers 1 to 5 are written out in words: one, two, three, four, five,
# then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
#
# If all the numbers from 1 to 1000 (one thousand) inclusive were written out 
# in words, how many letters would be used?
#
# NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
# forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20
# letters. The use of "and" when writing out numbers is in compliance with 
# British usage.

def number_to_words(number):
    """ Translates an integer to written words
    """
    word = ""
    number_string = str(number)

    spelled_numbers = ['','one','two','three','four','five',
                       'six' ,'seven','eight','nine','ten',
                       'eleven','twelve','thirteen','fourteen',
                       'fifteen','sixteen','seventeen','eighteen',
                       'nineteen']

    spelled_tens = ['','ten','twenty','thirty','forty','fifty',
                    'sixty','seventy','eighty','ninety']
    
    if len(number_string) == 4:
        word += (spelled_numbers[int(number_string[0])] + ' thousand')

    if len(number_string) >= 3:
        if int(number_string[-3]) > 0: 
            word += (' ' + spelled_numbers[int(number_string[-3])] + ' hundred')
            if int(number_string[-2]) > 0 or int(number_string[-1]) > 0:
                word += ' and'

    if len(number_string) >= 2:
        if int(number_string[-2:]) <= 19:
            word += (' ' + spelled_numbers[int(number_string[-2:])])
        elif int(number_string[-2:]) > 19:
            word += (' ' + spelled_tens[int(number_string[-2])] + 
                     ' ' + spelled_numbers[int(number_string[-1])])

    if len(number_string) == 1:
        word += (spelled_numbers[number])

    return word

def count(total):
    """ Count characters in all written numbers up to total
    """
    sum = 0
    for i in range(1,total + 1):
        word = number_to_words(i)
        word = word.replace(" ","")
        print(word)
        sum += len(word)
    return sum

print(count(1000))
