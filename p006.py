total = 100

def sum_of_squares(x):
    sum = 0
    for i in range(1,x+1):
        sum += i**2
        print(sum)
    return sum

def square_of_sums(x):
    sum = 0
    for i in range(1,x+1):
        sum += i
    return sum**2

print(str(sum_of_squares(100)-square_of_sums(100)))
