set1 = []

####### Turn file into a grid of ints #######
with open ('p011.txt') as file:
    lines = file.read().splitlines()            # Split file into list of lines
    lines[:] = [x.split(' ') for x in lines]    # Split each list into sublist of items
    lines = [[int(x) for x in l] for l in lines]# Turn all items into ints

def max_prod(list_x):
# Finds greatest product of any 4 numbers in a row
    max_product = 0
    for x in range(len(list_x)):    # Multiply row
        for y in range(len(list_x[x])-4):
            product = (list_x[x][y] * list_x[x][y+1] *
                       list_x[x][y+2] * list_x[x][y+3])
            if product > max_product:
                max_product = product

    for x in range(len(list_x)-4):  # Multiply down right
        for y in range(len(list_x[x])-4):
            product = (list_x[x][y] * list_x[x+1][y+1] *
                       list_x[x+2][x+2] * list_x[x+3][y+3])
            if product > max_product:
                max_product = product

    for x in range(4,len(list_x)):  # Multiply up right
        for y in range(len(list_x[x])-4):
            product = (list_x[x][y] * list_x[x-1][y+1] *
                       list_x[x-2][y+2] * list_x[x-3][y+3])
            if product > max_product:
                max_product = product

    for x in range(len(list_x)-4):  # Multiply column
        for y in range(len(list_x[x])-4):
            product = (list_x[x][y] * list_x[x+1][y] *
                       list_x[x+2][y] * list_x[x+3][y])
            if product > max_product:
                max_product = product

    return max_product

x = max_prod(lines)
print(x)
